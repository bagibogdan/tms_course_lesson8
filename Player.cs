﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player
{
    public class Player
    {
        protected string _name;
        protected Weapon _weapon;
        protected int _health;
        private int _maxHealth = 700;

        public Player()
        {
            _name = "Default";
            _health = 1;
            _weapon = new EmptyWeapon();
        }

        public Player(string name, Weapon weapon)
        {
            _name = name;
            _health = 500;
            _weapon = weapon;
        }

        public int Health
        {
            get
            {
                return _health;
            }
        }

        public int DecreaseHealth
        {
            set
            {
                _health -= value;
                if (_health < 0)
                {
                    _health = 0;
                }
            }
        }

        public int IncreaseHealth
        {
            set
            {
                _health += value;
                if (_health > _maxHealth)
                {
                    _health = _maxHealth;
                }
            }
        }

        public void ChangeWeapon(Weapon weapon)
        {
            Console.Write($"{_name} is changed weapon from {_weapon.Name} ");

            _weapon = weapon;

            Console.WriteLine($"to {weapon.Name}.");
            Console.WriteLine();
        }

        public void MakeDamage(Player injuredPlayer)
        {
            Console.WriteLine($"{_name} is damaged {injuredPlayer._name}.");
            injuredPlayer.TakeDamage(_weapon.Damage);
            Console.WriteLine($"{injuredPlayer._name} health is {injuredPlayer.Health}.");
            Console.WriteLine();
        }

        public void TakeDamage(int damage)
        {
            this.DecreaseHealth = damage;
            if (this.Health == 0)
            {
                _weapon = new EmptyWeapon();
            }
        }

        public void ShowPlayerInfo()
        {
            Console.WriteLine($"Player name: {_name}");
            Console.WriteLine($"Player health: {_health} HP");
            _weapon.ShowWeaponInfo();
        }
    }
}
