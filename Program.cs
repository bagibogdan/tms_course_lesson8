﻿using System;

namespace Player
{
    class Program
    {
        static void Main(string[] args)
        {
            Player criminal = new Player("Criminal", new Knife());
            Player policeman = new Player("Policeman", new Pistol());
            Player sniper = new Player("Sniper", new Gun());

            criminal.ShowPlayerInfo();
            policeman.ShowPlayerInfo();
            sniper.ShowPlayerInfo();

            criminal.MakeDamage(policeman);

            sniper.MakeDamage(criminal);

            criminal.ChangeWeapon(new Gun());
            criminal.MakeDamage(policeman);
            criminal.MakeDamage(sniper);

            policeman.MakeDamage(criminal);
            sniper.MakeDamage(criminal);
            sniper.MakeDamage(criminal);
            policeman.MakeDamage(criminal);

            criminal.ShowPlayerInfo();
            policeman.ShowPlayerInfo();
            sniper.ShowPlayerInfo();

            Console.ReadKey();
        }
    }
}
