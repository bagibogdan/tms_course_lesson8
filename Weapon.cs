﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player
{
    public abstract class Weapon
    {
        protected string _name;
        protected int _damage;
        protected int _distanse;

        public Weapon()
        {
            _name = "None";
            _damage = 0;
            _distanse = 0;
        }

        public virtual int Damage
        {
            get
            {
                return _damage;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public virtual void ShowWeaponInfo()
        {
            Console.WriteLine($"Weapon is {_name}");
            Console.WriteLine($"Damage: {_damage}");
            Console.WriteLine($"Distanse: {_distanse}");
            Console.WriteLine();
        }

        public virtual void ChangeBulletsCount(int bulletsOff)
        {
        }

        public virtual void ShowBulletsCount()
        {
        }
    }

    public class EmptyWeapon : Weapon
    {
        public EmptyWeapon()
        {
            _name = "None";
            _damage = 0;
            _distanse = 0;
        }
    }

    public class Knife : Weapon
    {
        public Knife()
        {
            _name = "Knife";
            _damage = 120;
            _distanse = 1;
        }
    }

    public abstract class Firearm : Weapon
    {
        protected int _bullets;
        protected int _bulletsOffPerShot;

        public override void ChangeBulletsCount(int bulletsOff)
        {
            _bullets -= bulletsOff;
        }

        public override int Damage
        {
            get
            {
                if (_bullets > 0)
                {
                    ChangeBulletsCount(_bulletsOffPerShot);
                    ShowBulletsCount();
                    return _damage;
                }
                else
                {
                    Console.WriteLine("Bullets count is zero. Damage not made.");
                    return 0;
                }

            }
        }

        public override void ShowWeaponInfo()
        {
            Console.WriteLine($"Weapon is {_name}");
            Console.WriteLine($"Damage: {_damage}");
            Console.WriteLine($"Distanse: {_distanse}");
            Console.WriteLine($"Bullets: {_bullets}");
            Console.WriteLine();
        }

        public override void ShowBulletsCount()
        {
            Console.WriteLine($"Bullets: {_bullets}");
        }
    }

    public class Pistol : Firearm
    {
        public Pistol()
        {
            _name = "Pistol";
            _damage = 80;
            _distanse = 80;
            _bullets = 10;
            _bulletsOffPerShot = 1;
        }
    }

    public class Gun : Firearm
    {
        public Gun()
        {
            _name = "Gun";
            _damage = 200;
            _distanse = 100;
            _bullets = 4;
            _bulletsOffPerShot = 2;
        }
    }
}
